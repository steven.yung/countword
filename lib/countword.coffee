CountwordView = require './countword-view'
{CompositeDisposable} = require 'atom'

module.exports = Countword =
  countwordView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @countwordView = new CountwordView(state.countwordViewState)
    @modalPanel = atom.workspace.addModalPanel(item: @countwordView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'countword:toggle': => @toggle()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @countwordView.destroy()

  serialize: ->
    countwordViewState: @countwordView.serialize()

  toggle: ->
    if @modalPanel.isVisible()
      @modalPanel.hide()
    else
      editor = atom.workspace.getActiveTextEditor()
      words = editor.getText().split(/\s+/).length
      @countwordView.setCount(words)
      @modalPanel.show()
